<?php

    include_once ("../Modelos/Usuario.php");

    class Login
    {
        private $email;
        private $password;

        function __construct($email, $password)
        {
            $this->email = $email;
            $this->password = $password;
        }

        function ingresar ()
        {
            $usuario = new Usuario ();
            return $usuario->ingresar();
        }

    }

    $login = new Login ($_POST["email"], $_POST["password"]);
    echo json_encode($login->ingresar());

?>