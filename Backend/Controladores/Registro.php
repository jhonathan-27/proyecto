<?php

    include_once ("../Modelos/Usuario.php");

    class Registro
    {
        private $nombre;
        private $email;
        private $cedula;
        private $genero;
        private $fecha;
        private $password;
        private $pais_residencia;
        private $ciudad_residencia;

        function __construct($nombre, $email, $cedula, $genero, $fecha, $password, $pais_residencia, $ciudad_residencia)
        {
            $this->nombre = $nombre;
            $this->email = $email;
            $this->cedula = $cedula;
            $this->genero = $genero;
            $this->fecha = $fecha;
            $this->password = $password;
            $this->pais_residencia = $pais_residencia;
            $this->ciudad_residencia = $ciudad_residencia;
            
        }

        function registrarse ()
        {
            $usuario = new Usuario ();
            return $usuario->registrarse( $this->nombre, $this->email, $this->cedula,  $this->genero,  $this->fecha, $this->password, 
            $this->pais_residencia, $this->ciudad_residencia);
        }

    }

    $registro = new Registro ($_POST["nombre"], $_POST["email"], $_POST["cedula"], $_POST["genero"], $_POST["fecha"], $_POST["password"], $_POST["pais_residencia"], $_POST["ciudad_residencia"]);
    echo json_encode($registro->registrarse());

?>